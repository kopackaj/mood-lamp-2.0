# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka                                        | vyjádření                       |
| :-------------------------------------------- | :------------------------------ |
| jak dlouho mi tvorba zabrala - **čistý čas**  | cca 25 hodin (10h - vyhledávání; 5h - dokumentace, video)                                |
| odkud jsem čerpal inspiraci                   | *nikde, jen z google obrázků :DD*                           |
| odkaz na video                                | https://youtu.be/4pP3vJw-fSo                     |
| jak se mi to podařilo rozplánovat             | perfektně, brzo hotovo                                |
| proč jsem zvolil tento design                 | kvůli dostupnosti materiálů                                |
| zapojení                                      | https://gitlab.spseplzen.cz/kopackaj/mood-lamp-2.0/-/blob/main/dokumentace/schema/foto_schema_zapojeni.jpg          |
| z jakých součástí se zapojení skládá          | DHT22, 10KΩ R., ESP8266, WS2812, kabely                                 |
| realizace                                     | https://gitlab.spseplzen.cz/kopackaj/mood-lamp-2.0/-/blob/main/dokumentace/fotky/produkt.jpg |
| UI                                            | https://gitlab.spseplzen.cz/kopackaj/mood-lamp-2.0/-/blob/main/dokumentace/schema/UI.jpg                |
| co se mi povedlo                              | originální způsob předávání RGB dat (bez JSONu)                               |
| co se mi nepovedlo/příště bych udělal/a jinak | zapojení, pájení                                |
| zhodnocení celé tvorby (návrh známky)         | vše splněno a včas, jen by to chtělo lepší zapojení, 9/10;                                |